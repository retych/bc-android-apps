package cz.retych.soundgenerator;

import android.annotation.SuppressLint;
import android.graphics.Color;
import android.media.AudioFormat;
import android.media.AudioManager;
import android.media.AudioTrack;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;

import java.util.ArrayList;
import java.util.List;

public class soundGenerator extends AppCompatActivity {

    // 8000 Hz zvládne reproduktor telefonu
    private final int maxFrequency = 8000; // hz
    private final int duration = 300; // ms multiplier
    private final int sampleRate = 10*maxFrequency; // Odpovídá 1 sekundě
    private final int numSamples = duration * sampleRate / 1000;
    private double sample[] = new double[numSamples];
    private AudioTrack audioTrack;

    private boolean m_stop = false;
    private Thread soundThread;
    private byte generatedSnd[] = new byte[2 * numSamples];

    // Komponenty UI
    private LinearLayout layout;
    private static LineChart mChart;

    // Dodatečné proměnné
    protected int colorWhite = R.color.whiteShadow;
    private static boolean graphCreate = true;
    private boolean playing = false;

    // Zakrytí warningu v kódu pro View.OnTouchListener()
    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final TextView pointerOneStatusTextView = findViewById(R.id.multi_touch_pointer1);

        final TextView pointerTwoStatusTextView = findViewById(R.id.multi_touch_pointer2);

        audioTrack = new AudioTrack(AudioManager.STREAM_MUSIC,
                sampleRate,
                AudioFormat.CHANNEL_OUT_MONO,
                AudioFormat.ENCODING_PCM_16BIT, numSamples,
                AudioTrack.MODE_STREAM);
        audioTrack.setPositionNotificationPeriod(numSamples);
        audioTrack.setNotificationMarkerPosition(numSamples);
        audioTrack.setPlaybackPositionUpdateListener(new AudioTrack.OnPlaybackPositionUpdateListener(){

            @Override
            public void onMarkerReached(AudioTrack track) {

            }

            @Override
            public void onPeriodicNotification(AudioTrack track) {
                if(!playing) track.stop();
            }
        });
        start();

        mChart = findViewById(R.id.mChart);
        mChart.setTouchEnabled(false);
        setUpmCharts();

        layout = findViewById(R.id.multi_touch_layout);

        layout.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                int totalPointerCount = motionEvent.getPointerCount();

                for(int i=0;i<totalPointerCount;i++) {
                    double x = motionEvent.getX(i);
                    double y = motionEvent.getY(i);

                    // hodnoty 0 až 10
                    int amp = (int) ( ( ((view.getHeight()-motionEvent.getY(i))/view.getHeight())* 100 )+1 )/totalPointerCount;

                    // hodnoty 0 až maxfreq
                    int freq = (int) (motionEvent.getX(i) / view.getWidth() * maxFrequency);

                    // Pojistka
                    if(motionEvent.getY(i) <=0 || motionEvent.getY(i)>view.getHeight()) amp = 1;
                    if(motionEvent.getX(i) <=0 || motionEvent.getX(i)>view.getWidth()) freq = 50;

                    StringBuilder pointerStatusBuf = new StringBuilder();

                    // Get pointer id.
                    int pointerId = motionEvent.getPointerId(i);
                    pointerStatusBuf.append("Pointer Id : ");
                    pointerStatusBuf.append(pointerId);
                    pointerStatusBuf.append(" . ");

                    // Get pointer action.
                    pointerStatusBuf.append("Action : ");

                    int action = motionEvent.getAction();
                    pointerStatusBuf.append(action + "??");

                    if(action == MotionEvent.ACTION_DOWN) {
                        //start();
                        playing = true;
                        if(i == 0) {
                            //sample = getPeriodThenAppend(freq, amp, numSamples);
                            sample = calcAllUsingMath(freq, amp);
                            sample = applyRampOfType(sample, 2, true, true);

                        } else {
                            //double[] samps = getPeriodThenAppend(freq, amp, numSamples);
                            double[] samps = calcAllUsingMath(freq, amp);
                            for(int j=0;j<numSamples;j++) {
                                sample[j] += samps[j];
                                sample = applyRampOfType(sample, 2, true, true);
                            }
                        }
                        pointerStatusBuf.append("Down. ");
                    } else if(action == MotionEvent.ACTION_MOVE) {
                        if(i == 0) {
                            sample = calcAllUsingMath(freq, amp);
                            sample = applyRampOfType(sample, 2, true, true);
                        } else {
                            double[] samps = calcAllUsingMath(freq, amp);
                            for(int j=0;j<numSamples;j++) {
                                sample[j] += samps[j];
                                sample = applyRampOfType(sample, 2, true, true);
                            }
                        }
                        pointerStatusBuf.append("Move. ");
                    } else if(action == MotionEvent.ACTION_UP) {
                        //stop();
                        playing = false;
                        pointerStatusBuf.append("Up. ");
                    } else if(action == MotionEvent.ACTION_POINTER_DOWN) {
                        //start();
                        // Detekování více stisků
                        pointerStatusBuf.append("POINTER_DOWN . ");
                    }

                    if(i == totalPointerCount-1) genTone();
                    // Get action index.
                    int actionIndex = motionEvent.getActionIndex();
                    pointerStatusBuf.append("Action Index : ");
                    pointerStatusBuf.append(actionIndex);
                    pointerStatusBuf.append(". \n");


                    pointerStatusBuf.append("\n FREQ : amp \n");
                    pointerStatusBuf.append(freq);
                    pointerStatusBuf.append(" : ");
                    pointerStatusBuf.append(amp);

                    if(pointerId == 0) {
                        // First textview display first touch pointer status info.
                        pointerOneStatusTextView.setText(pointerStatusBuf.toString());
                    } else {
                        // Second textview display second touch pointer status info.
                        pointerTwoStatusTextView.setText(pointerStatusBuf.toString());
                    }
                }
                // Tell android system event has been consumed by this listener.
                return true;
            }
        });
    }

    private double[] calcAllUsingMath(double freq, int amp) {
        // Záleží na počtu vzorků: 20 až 130 ms
        long startTime = System.currentTimeMillis();
        double samples[] = new double[numSamples];
        for (int i = 0; i < numSamples; i++) {
            samples[i] = ((double) amp / 100) * Math.sin(2 * Math.PI * i/sampleRate * (freq));
        }
        long mathTime = System.currentTimeMillis() - startTime;
        System.out.print("\nGenToneMath:" + mathTime + "ms\n");
        return samples;
    }

    void genTone() {
        long startTime = System.currentTimeMillis();
        //Arrays.fill(sample, 0);

        // Pokud vlákno již běží, nevytvoří se další
        if(graphCreate) {
            new renderChart(sample).execute();
            graphCreate = false;
        }

        // Vytvoření 16 bit vzorků
        int idx = 0;
        for (final double dVal : sample) {
            // scale to maximum amplitude
            final short val = (short) ((dVal * 32767));
            // in 16 bit wav PCM, first byte is the low order byte
            generatedSnd[idx++] = (byte) (val & 0x00ff);
            generatedSnd[idx++] = (byte) ((val & 0xff00) >>> 8);
        }

        long finalTime = System.currentTimeMillis() - startTime;
        System.out.print("\nGenTone: " + finalTime + "ms \n");
    }

    Runnable m_noiseGenerator = new Runnable() {
        public void run()
        {
            Thread.currentThread().setPriority(Thread.MIN_PRIORITY);

            while(!m_stop)
            {
                try {
                    if(playing) {

                        long startTime = System.currentTimeMillis();
                        audioTrack.write(generatedSnd, 0, generatedSnd.length);
                        if (audioTrack.getPlayState() == AudioTrack.PLAYSTATE_STOPPED) {
                            audioTrack.play();
                        }

                        long finish = System.currentTimeMillis() - startTime;
                        System.out.print("\naudioTrackThread: WRITTEN IN " + finish + "ms\n");

                    }
                } catch (Exception ex) {
                    System.out.print("THREAD ERROR " + ex);
                }
            }
        }
    };

    void start() {
        try {
            m_stop = false;
            audioTrack.play();
            if(soundThread == null) {
                soundThread = new Thread(m_noiseGenerator);
                soundThread.start();
            }
        } catch (Exception ex) {
            System.out.print("START ERROR " + ex);
        }
    }

    void stop() {
        try {
            m_stop = true;
            audioTrack.stop();
            audioTrack.flush();
            soundThread.join();
            soundThread = null;
        } catch (Exception ex) {
            System.out.print("STOP ERROR " + ex);
        }
    }

    private void setUpmCharts() {
        // Main pro nastavení Charts
        setUpSpecificmChart(mChart, numSamples, 1.0f, true);
    }

    private void setUpSpecificmChart(LineChart chart, int XAxisMax, float YAxisMax, boolean negativeValues) {
        // Nastavení stylů a barev mChartu
        chart.setScaleEnabled(false);
        chart.getLegend().setEnabled(false);
        chart.setDescription(null);
        // chart.animateXY(1000, 1000);
        XAxis xAxis = chart.getXAxis();
        YAxis leftAxis = chart.getAxisLeft();
        YAxis rightAxis = chart.getAxisRight();
        if(negativeValues) {
            rightAxis.setAxisMinimum(-YAxisMax);
            rightAxis.setAxisMaximum(YAxisMax);
            leftAxis.setAxisMinimum(-YAxisMax);
            leftAxis.setAxisMaximum(YAxisMax);
        } else {
            rightAxis.setAxisMinimum(0);
            rightAxis.setAxisMaximum(YAxisMax);
            leftAxis.setAxisMinimum(0);
            leftAxis.setAxisMaximum(YAxisMax);
        }

        xAxis.setAxisMinimum(0);
        //xAxis.setAxisMinimum(XAxisMax-300);
        xAxis.setAxisMaximum(XAxisMax);
        leftAxis.setTextColor(Color.RED);
        xAxis.setTextColor(Color.RED);
        leftAxis.setTextSize(10f);
        xAxis.setTextSize(10f);
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);

        rightAxis.setDrawLabels(false);
        rightAxis.setDrawGridLines(false);
        leftAxis.setDrawAxisLine(true);
        leftAxis.setDrawGridLines(true);
        xAxis.setDrawAxisLine(true);
        xAxis.setDrawGridLines(true);
        chart.setGridBackgroundColor(colorWhite);

        // Nahrani hodnot do grafu - default
        List<Entry> chartPoints = new ArrayList<>();
        chartPoints.add(new Entry(0, 0));
        LineDataSet dataSet = new LineDataSet(chartPoints, "");
        dataSet.disableDashedLine();
        dataSet.setColor(Color.GREEN);
        dataSet.setDrawCircles(false);
        LineData lineData = new LineData(dataSet);
        chart.setData(lineData);

        chart.invalidate(); // refresh
    }

    public static class renderChart extends AsyncTask<Void, LineDataSet, Void> {
        private double[] data;
        private long startTime;

        private renderChart(double[] samples) {
            this.data = samples;
            this.startTime = System.currentTimeMillis();
        }

        @Override
        protected Void doInBackground(Void... arg0) {
            try {
                List<Entry> mChartPoints = new ArrayList<>();
                for (int i=0;i<data.length;i++) {
                    mChartPoints.add(new Entry(i, (float) data[i]));
                }

                // Seskupení dat pro Charts
                LineDataSet setComp1 = new LineDataSet(mChartPoints, "");
                publishProgress(setComp1);
            } catch (Throwable t) {
                t.printStackTrace();
                Log.e("GraphRender", "GraphRender failed");
            }
            return null;
        }

        @Override
        protected void onProgressUpdate(LineDataSet... LineDataSets) {
            try {
                LineDataSets[0].disableDashedLine();
                LineDataSets[0].setColor(Color.WHITE);
                LineDataSets[0].setDrawCircles(false);

                LineData lineData = new LineData(LineDataSets[0]);
                mChart.setData(lineData);
                mChart.invalidate(); // refresh
                long finish = System.currentTimeMillis() - this.startTime;
                System.out.print("\ngraphThread: printed in: " + finish + "ms \n");
                graphCreate = true;
            } catch (Throwable e) {
                e.printStackTrace();
                Log.e("Graph", "Selhalo vykresleni");
            }
        }
    }

    private double[] applyRampOfType(double[] data, int type, boolean onStart, boolean onEnd) {
        double length = data.length;
        int deep = 2000;
        if(onStart) {
            for (int i = 0; i <= deep; i++) {
                switch (type) {
                    case 0: {
                        // Ramp
                        data[i] *= ((double)i / deep);
                    }
                    case 1: {
                        // logRamp
                        data[i] *= Math.log10(1 + ((double)i / deep) * 9);
                        break;
                    }
                    case 2: {
                        // ExpRamp
                        double value = Math.E + 1;
                        data[i] *= Math.exp(((double)i / deep) * value - value);
                    }
                }
            }
        }
        if(onEnd) {
            for(int i = (int)length-deep; i<length;i++) {
                switch(type) {
                    case 0: {
                        // Ramp
                        data[i] *= ((length - i) / deep);
                    }
                    case 1: {
                        // logRamp
                        data[i] *= Math.log10(1 + ((length - i) / deep) * 9);
                        break;
                    }
                    case 2: {
                        // ExpRamp
                        double value = Math.E + 3;
                        data[i] *= Math.exp(((length - i) / deep) * value - value);
                    }
                }
            }
        }

        return data;
    }

    @Override
    protected void onResume() {
        super.onResume();
        start();
    }

    @Override
    protected void onPause() {
        super.onPause();
        stop();
    }
}
