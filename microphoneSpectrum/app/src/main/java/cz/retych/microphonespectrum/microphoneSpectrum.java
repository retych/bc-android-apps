package cz.retych.microphonespectrum;

import android.Manifest;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.media.AudioFormat;
import android.media.AudioRecord;
import android.media.MediaRecorder;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class microphoneSpectrum extends AppCompatActivity implements View.OnClickListener {

    // Proměnné potřebné pro setUp audioRecord
    // FFT generuje oboustranné spektrum -> 2*
    //private int frequency = 10000*2;
     private int frequency = 22000;
    private int sampleCount = 128*8; //512
    private AudioRecord audioRecord;
    private RecordAudioTask recordTask;
    private float[] buffer;
    private boolean started = false;
    private final int REQUEST_PERMISSION = 200;

    // Proměnné potřebné pro výpočet FFT
    private FFT fft;
    private double[] fftX;
    private double[] fftY;

    // Proměnné pro průběh zvuku v čase (imageView vykreslovaný za pomocí Paint)
    // PS: Pod grafy
    private Paint paintSnd;
    private Canvas canvasSnd;
    private int countToRefresh=1000;
    private int imageViewHeight=250;
    private int counterToRefresh=0;
    private double micMax=0;

    // Komponenty UI
    private ImageView imageView;
    private TextView textView;
    private Button startStopButton;
    private LineChart mChart;
    private LineChart mChart2;
    private Switch switch1;
    private SeekBar seekBar;
    private TextView seekBarTextView;

    // Kalibrace mikrofonu
    private double Calibration = 0; //dB

    // Dodatečné proměnné
    protected int colorWhite = R.color.whiteShadow;
    protected int colorBlack = R.color.colorPrimary;
    private boolean onePush = false;
    private int seekBarValue = 0;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_microphone_spectrum);

        fftX = new double[sampleCount];
        fftY = new double[sampleCount];
        buffer = new float[sampleCount];
        fft = new FFT(sampleCount);

        // Získání šířky displeje
        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        countToRefresh = metrics.widthPixels;

        // Přiřezení komponent UI
        startStopButton = this.findViewById(R.id.start_stop_btn);
        startStopButton.setOnClickListener(this);
        mChart = findViewById(R.id.mChart);
        mChart2 = findViewById(R.id.mChart2);
        imageView = this.findViewById(R.id.imageView);
        textView = this.findViewById(R.id.textViewxx);
        switch1 = this.findViewById(R.id.switch1);
        seekBar = this.findViewById(R.id.seekBar);
        seekBar.setVisibility(View.INVISIBLE);
        seekBarTextView = this.findViewById(R.id.seekBarTextView);

        // Nastavení stylů, defaultních hodnot a barev
        this.setUpmCharts();
        this.setUpSndInTime();
        this.setUpAxisTitles();

        // Ověření přístupu - permissions
        if(this.getPermission()) this.setUpMicrophone();
    }

    public class RecordAudioTask extends AsyncTask<Void, LineDataSet, Void> {
        private int maxIndex;
        //private double logMinInput = 0.0000005;
        private double logMinInput = 2/(65536.0);
        private  double logMinValue = -20*Math.log10(logMinInput);
        private double calibrationValue = Math.pow(10, Calibration/20);
        private double logMax=-100;
        private double yMax=0;
        private int onPushCounter;

        @Override
        protected Void doInBackground(Void... arg0) {
            try {
                audioRecord.startRecording();
                onPushCounter = 5 + seekBarValue;
                while (started) {
                    // Získání vzorků
                    int bufferReadResult = audioRecord.read(buffer, 0, sampleCount, AudioRecord.READ_BLOCKING);

                    //System.out.println("\n result:"+ bufferReadResult + "\n");
                    // Reset hodnot
                    micMax = 0;
                    maxIndex = 0;
                    yMax = 0;
                    logMax = 0;
                    List<Entry> mChartInputPoints = new ArrayList<>();

                    // Úprava vstupním hodnot na rozsah -1 až 1
                    for (int i = 0; i < sampleCount && i < bufferReadResult; i++) {
                        // Testovací vzorek:
                        //double value = 0.3*Math.sin(2*Math.PI*500*i / sampleCount) + 0.6*Math.sin(2*Math.PI*1500*i / sampleCount) + 0.1*buffer[i];
                        //double value = 1*Math.sin(2*Math.PI*500*i / sampleCount);
                        double value = buffer[i];
                        fftX[i] = value;
                        fftY[i] = 0;

                        if (micMax < value) micMax = value;
                        mChartInputPoints.add(new Entry(i, (float) (value)));
                    }
                    // Aplikace Fast Fourier tranform
                    fft.fft(fftX, fftY);
                    // OUTPUT: fftX = fftXvalue ; fftY = fftYvalue

                    List<Entry> mChartFFTPoints = new ArrayList<>();
                    //List<Entry> mChartFFTPoints2 = new ArrayList<>();
                    for (int i = 1; i < sampleCount; i++) {

                        // Přepočet hodnot
                        float x = ((float)i) * frequency / sampleCount;
                        double amplitude = Math.sqrt(fftY[i]*fftY[i]+fftX[i]*fftX[i]);
                        //double phase = Math.atan2(fftY[i], fftX[i]);

                        // Amplituda sinusovky je od -1 do 1 = 2
                        // FFT oboustranné spektrum => 0 až 0,5 => *2 = 0 až 1
                        amplitude = 2*amplitude/(double)(sampleCount);


                        // Ošetření minimální hodnoty logaritmu
                        if(amplitude < logMinInput) amplitude = logMinInput;

                        if (yMax < amplitude) yMax = amplitude;

                        // Výpočet Y hodnoty 0 až 1
                        // Nedělím už 2 protože mám energii (0-1), ne (0-2)
                        double y = 20*Math.log10(amplitude) + Calibration + logMinValue;

                        // Stejný výsledek
                        //double y = 20*Math.log10(amplitude * calibrationValue) + logMinValue;

                        // Zjištění maximální hodnoty
                        if (logMax < y) {
                            logMax = y;
                            maxIndex = i;
                        }
                        if(y<0) y = 0;
                        // Přídání X a Y do grafu.
                        mChartFFTPoints.add(new Entry(x, (float) y));
                        //mChartFFTPoints2.add(new Entry(x, (float) y2));
                    }

                    // Seskupení dat pro Charts
                    LineDataSet setComp1 = new LineDataSet(mChartInputPoints, "");
                    LineDataSet setComp2 = new LineDataSet(mChartFFTPoints, "");
                    //LineDataSet setComp3 = new LineDataSet(mChartFFTPoints2, "");

                    // Odeslání -> onProgressUpdate
                    publishProgress(setComp1, setComp2/*, setComp3*/);
                    //Thread.sleep(5);

                    // OnePush tlačítko
                    if (onePush) onPushCounter--;
                    if(onPushCounter == 0) {
                        started = false;
                        break;
                    }
                }
                // END OF WHILE
                if(audioRecord != null) audioRecord.stop();
            } catch (Throwable t) {
                t.printStackTrace();
                Log.e("AudioRecord", "Recording Failed");
            }
            return null;
        }

        @Override
        protected void onProgressUpdate(LineDataSet... LineDataSets) {
            try {
                int value = maxIndex;

                // Zavolání funkcí
                this.drawSndInTime();
                this.renderChart(LineDataSets[0], LineDataSets[1] /*, LineDataSets[2] */);

                // Tisk textu
                String text = " Celkem: " + sampleCount + " Vzorků";
                text += String.format(Locale.getDefault(),
                        "\n MICmax: %.3f mV\n fftAmp: %.3f mV" +
                                "\n logMax: %.1f dB" +
                                "\n LogMinValue: %.3f dB\n LogMinInput: %.10f" +
                                "\n Calibration: %.2f dB",
                                micMax , yMax, logMax, logMinValue, logMinInput, Calibration);

                textView.setText(text);

                if(onPushCounter == 0)
                    startStopButton.setBackgroundColor(getColor(R.color.darkRed));
            } catch (Throwable e) {
                e.printStackTrace();
                Log.e("Graph", "Selhalo vykresleni");
            }
        }

        private void drawSndInTime() {
            // Vykreslení maximální hodnoty z audia vzorku do imageView -> pod grafy
            if(counterToRefresh >= countToRefresh)
            {
                System.out.println("\n refresh!\n");
                counterToRefresh = 0;
                canvasSnd.drawColor(getColor(colorBlack));
            }

            float Maximum = (float)((micMax)*imageViewHeight)+1;
            canvasSnd.drawLine(counterToRefresh, imageViewHeight/2f+Maximum, counterToRefresh, imageViewHeight/2f-Maximum, paintSnd);
            imageView.invalidate();
            counterToRefresh++;
        }

        private void renderChart(LineDataSet dataSet, LineDataSet dataSet2 /*, LineDataSet dataSet3 */) {
            // Nastavení vzhledu
            dataSet.disableDashedLine();
            dataSet.setColor(Color.WHITE);
            dataSet.setDrawCircles(false);
            dataSet2.disableDashedLine();
            dataSet2.setColor(Color.YELLOW);
            dataSet2.setDrawCircles(false);
            /*
            dataSet3.disableDashedLine();
            dataSet3.setColor(Color.WHITE);
            dataSet3.setDrawCircles(false);
            */
            ArrayList<ILineDataSet> dataSets = new ArrayList<>();
            dataSets.add(dataSet2);
            //dataSets.add(dataSet3);

            LineData lineData = new LineData(dataSet);
            LineData lineData2 = new LineData(dataSets);
            mChart.setData(lineData);
            mChart2.setData(lineData2);
            mChart.invalidate(); // refresh
            mChart2.invalidate(); // refresh
        }
    }

    private void setUpMicrophone() {
        // Nastavení mikrofonu
        int channelConfiguration = AudioFormat.CHANNEL_IN_MONO;
        int audioEncoding = AudioFormat.ENCODING_PCM_FLOAT;
        int bufferSize = AudioRecord.getMinBufferSize(frequency, channelConfiguration, audioEncoding);

        audioRecord = new AudioRecord( MediaRecorder.AudioSource.MIC, frequency,
                channelConfiguration, audioEncoding, bufferSize);
    }

    private void cancelRecording() {
        started = false;
        String text = "Start";
        startStopButton.setText(text);
        startStopButton.setBackgroundColor(getColor(R.color.darkRed));
        if(audioRecord != null) {
            audioRecord.stop();
            recordTask.cancel(true);
        }
    }

    private void startRecording() {
        this.setUpMicrophone();
        started = true;
        String text = "Stop";
        startStopButton.setText(text);
        startStopButton.setBackgroundColor(getColor(R.color.darkGreen));
        recordTask = new RecordAudioTask();
        recordTask.execute();
    }

    public void onClick(View arg0) {
        // On click event obrazovky
        onePush = switch1.isChecked();
        seekBarValue = seekBar.getProgress();
        if (started) {
                this.cancelRecording();
        } else {
            if(this.getPermission()) {
                this.startRecording();
            }
        }
        if(onePush) {
            String text = "Aktivovat burst";
            startStopButton.setText(text);
            seekBar.setVisibility(View.VISIBLE);
            String str = "" + seekBarValue;
            seekBarTextView.setText(str);
        } else {
            seekBar.setVisibility(View.INVISIBLE);
            seekBarTextView.setText("");
        }
        System.out.println("\n Clicked!\n");
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        cancelRecording();
    }

    private boolean getPermission() {
        try {
            // Permission
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.RECORD_AUDIO) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this, new String[]{
                        Manifest.permission.RECORD_AUDIO,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE
                }, REQUEST_PERMISSION);
                return false;
            }
            else return true;
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("\n !!!!!!!\n"+e.toString());
        }
        return false;
    }

    // Případ, kdy jsou permission zamítnuty
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if(requestCode == REQUEST_PERMISSION)
        {
            if(grantResults[0] != PackageManager.PERMISSION_GRANTED)
            {
                Toast.makeText(this, "Nelze použít mikrofon dokud aplikace nezíská oprávnění", Toast.LENGTH_SHORT).show();
                finish();
            }
        }
    }

    // *************************************************** //
    // *** Nastavení charts, os x a y a dalších stylů **** //
    // *************************************************** //

    private void SetUpYaxisTitle(String str, ImageView iView) {
        // Vytisknutí textu os pro Charts osa Y
        Canvas cv;
        Paint paint;
        Bitmap bm;
        int hw=800/2;
        float x, y;

        // Yaxis
        bm = Bitmap.createBitmap( 100, hw, Bitmap.Config.ARGB_8888);
        cv = new Canvas(bm);
        paint = new Paint();
        paint.setStyle(Paint.Style.FILL);
        //cv.drawColor(getColor(colorWhite));
        cv.drawPaint(paint);
        paint.setColor(Color.RED);
        paint.setTextSize(60);
        cv.save();
        x = 90;
        y = hw/2f+paint.measureText(str)/2;
        cv.rotate(-90, x, y);
        cv.drawText(str, x, y, paint);
        cv.restore();
        //ImageView Yaxis = this.findViewById(R.id.YaxisTitleImageView);
        iView.setImageBitmap(bm);
        //Yaxis.setBackgroundColor(Color.GREEN);
    }

    private void SetUpXaxisTitle(String str,  ImageView iView) {
        // Vytisknutí textu os pro Charts osa X
        Canvas cv;
        Paint paint;
        Bitmap bm;
        int width=800;
        float x, y;

        //Xaxis
        bm = Bitmap.createBitmap( width, 100, Bitmap.Config.ARGB_8888);
        cv = new Canvas(bm);
        paint = new Paint();
        paint.setStyle(Paint.Style.FILL);
        //cv.drawColor(getColor(colorWhite));
        cv.drawPaint(paint);
        paint.setColor(Color.RED);
        paint.setTextSize(60);
        cv.save();
        x = width/2f-paint.measureText(str)/2;
        y = 60;
        cv.drawText(str, x, y, paint);
        cv.restore();
        //ImageView iView = this.findViewById(R.id.XAxisTitleImageView);
        iView.setImageBitmap(bm);
        //Xaxis.setBackgroundColor(Color.GREEN);
    }

    private void setUpAxisTitles() {
        // Nastavení popisů grafů pro graf1
        ImageView xView1 = this.findViewById(R.id.XAxisTitleImageView);
        ImageView yView1 = this.findViewById(R.id.YaxisTitleImageView);
        SetUpXaxisTitle("Vzorky [-]", xView1);
        SetUpYaxisTitle("Amp [mV]", yView1);

        // Nastavení popisů grafů pro graf2
        ImageView xView2 = this.findViewById(R.id.XAxisTitleImageView2);
        ImageView yView2 = this.findViewById(R.id.YaxisTitleImageView2);
        SetUpXaxisTitle("Frekvence [Hz]", xView2);
        SetUpYaxisTitle("[dB]", yView2);

    }

    private void setUpSpecificmChart(LineChart chart, float XAxisMax, float YAxisMin, float YAxisMax, int negativeValues) {
        // Nastavení stylů a barev mChartu
        chart.setScaleEnabled(false);
        chart.getLegend().setEnabled(false);
        chart.setDescription(null);
        // chart.animateXY(1000, 1000);
        XAxis xAxis = chart.getXAxis();
        YAxis leftAxis = chart.getAxisLeft();
        YAxis rightAxis = chart.getAxisRight();
        if(negativeValues == 0) {
            rightAxis.setAxisMinimum(0);
            rightAxis.setAxisMaximum(YAxisMax);
            leftAxis.setAxisMinimum(0);
            leftAxis.setAxisMaximum(YAxisMax);
        } else if(negativeValues == 1) {
            rightAxis.setAxisMinimum(-YAxisMax);
            rightAxis.setAxisMaximum(YAxisMax);
            leftAxis.setAxisMinimum(-YAxisMax);
            leftAxis.setAxisMaximum(YAxisMax);
        } else {
            rightAxis.setAxisMinimum(YAxisMin);
            rightAxis.setAxisMaximum(YAxisMax);
            leftAxis.setAxisMinimum(YAxisMin);
            leftAxis.setAxisMaximum(YAxisMax);
        }

        xAxis.setAxisMinimum(0);
        xAxis.setAxisMaximum(XAxisMax);
        leftAxis.setTextColor(Color.RED);
        xAxis.setTextColor(Color.RED);
        leftAxis.setTextSize(10f);
        xAxis.setTextSize(10f);
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);

        rightAxis.setDrawLabels(false);
        rightAxis.setDrawGridLines(false);
        leftAxis.setDrawAxisLine(true);
        leftAxis.setDrawGridLines(true);
        xAxis.setDrawAxisLine(true);
        xAxis.setDrawGridLines(true);
        chart.setGridBackgroundColor(colorWhite);

        // Nahrani hodnot do grafu - default
        List<Entry> chartPoints = new ArrayList<>();
        chartPoints.add(new Entry(0, 0));
        LineDataSet dataSet = new LineDataSet(chartPoints, "");
        dataSet.disableDashedLine();
        dataSet.setColor(Color.GREEN);
        dataSet.setDrawCircles(false);
        LineData lineData = new LineData(dataSet);
        chart.setData(lineData);

        chart.invalidate(); // refresh
    }

    private void setUpmCharts() {
        // Main pro nastavení Charts
        setUpSpecificmChart(mChart, sampleCount,0, 1.0f, 1);
        setUpSpecificmChart(mChart2, frequency/2, 0, 100, 2);
        //setUpSpecificmChart(mChart2, frequency/2, -5, 5, 2);
    }

    private void setUpSndInTime() {
        // Nastavení vykreslování audia v závislosti na čase (imageView)
        Bitmap bitmapSnd = Bitmap.createBitmap(countToRefresh, imageViewHeight, Bitmap.Config.ARGB_8888);
        canvasSnd = new Canvas(bitmapSnd);
        paintSnd = new Paint();
        paintSnd.setColor(Color.GRAY);
        imageView.setImageBitmap(bitmapSnd);
    }


}

