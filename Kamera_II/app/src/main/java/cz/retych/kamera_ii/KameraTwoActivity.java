package cz.retych.kamera_ii;
import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.ImageFormat;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.SurfaceTexture;
import android.graphics.drawable.BitmapDrawable;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraCaptureSession;
import android.hardware.camera2.CameraCharacteristics;
import android.hardware.camera2.CameraDevice;
import android.hardware.camera2.CameraManager;
import android.hardware.camera2.CaptureRequest;
import android.hardware.camera2.TotalCaptureResult;
import android.hardware.camera2.params.StreamConfigurationMap;
import android.media.ImageReader;
import android.os.Handler;
import android.os.HandlerThread;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Size;
import android.util.SparseIntArray;
import android.view.Surface;
import android.view.TextureView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.Random;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class KameraTwoActivity extends AppCompatActivity implements View.OnClickListener {

    private TextureView privateTextureView;
    private ImageView privateImageView;
    private TextView privateTextView;
    private TextView privateTextViewTimers;
    private TextView privateTextViewLeftText;

    // Potřebné proměnné pro práci s threadem
    private Thread imageThread;
    public static boolean runImfProcess = false;
    public static boolean imgReady = false;
    public static boolean printImg = false;
    public static boolean debugModeToggleCircles = false;
    public static boolean debugModeToggleLines = false;
    public static boolean debugModeToggleStepByStep = false;
    public static int objectsCount = 0;
    public static int linesCount = 0;
    public static Bitmap imfToProcess;
    public static Bitmap imgToPrint;
    public static Lock locker;
    public static long finishedIn = 0;

    private long finishedInMin = 100000;
    private long finishedInMax = 0;

    public static Matrix imgMatrix;
    public static Bitmap transparentBitmap;

    // Potřebné proměnné pro práci s kamera2 api
    private String cameraId;
    private CameraDevice cameraDevice;
    private CameraCaptureSession cameraCaptureSessions;
    private CaptureRequest.Builder captureRequestBuilder;
    private Size cameraDimension;
    private static final int REQUEST_CAMERA_PERMISSION = 200;
    private Handler mBackgroundHandler;
    private HandlerThread mBackgroundThread;

    // Demo
    private ImageView demoImageView;
    private Bitmap demoImage;


    // Vytvoření tvz. call-backu(zpětné odezvy) pro práci s kamera2 api
    CameraDevice.StateCallback stateCallback = new CameraDevice.StateCallback() {
        @Override
        public void onOpened(@NonNull CameraDevice camera) {
            cameraDevice = camera;
            setUpCameraPreview();
        }

        @Override
        public void onDisconnected(@NonNull CameraDevice cameraDevice) {
            cameraDevice.close();
        }

        @Override
        public void onError(@NonNull CameraDevice cameraDevice, int i) {
            cameraDevice.close();
            cameraDevice = null;
        }
    };

    //Funkce onCreate volající se při startu programu - inicializace
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_kamera_two);

        privateTextureView = findViewById(R.id.textureView);
        privateTextureView.setOnClickListener(this);
        privateImageView = findViewById(R.id.imageView);
        demoImageView = findViewById(R.id.demoImageView);
        demoImageView.setVisibility(View.INVISIBLE);
        /*
        // Demo - dále odkomentovat řádek 223
        demoImage = BitmapFactory.decodeResource(getResources(), R.drawable.kamera2_demoimg);
        Matrix matrix = new Matrix();
        matrix.postRotate(270);
        Bitmap bm = Bitmap.createBitmap(demoImage, 0, 0,demoImage.getWidth(),demoImage.getHeight(), matrix, true);
        demoImageView.setImageBitmap(bm);
        demoImageView.setVisibility(View.VISIBLE);
        */

        assert privateTextureView != null;
        privateTextView = (TextView) findViewById(R.id.textViewCenter);
        privateTextViewTimers = (TextView) findViewById(R.id.textViewTimers);
        privateTextViewLeftText = (TextView) findViewById(R.id.textViewLeftText);

        // Vytvoření posluchače pro privateTextureView a button
        privateTextureView.setSurfaceTextureListener(textureListener);

        // Defaultní hodnoty
        runImfProcess = false;
        imgReady = false;
        debugModeToggleCircles = false;
        debugModeToggleLines = false;
        debugModeToggleStepByStep = false;
        objectsCount = 0;
        linesCount = 0;
        imfToProcess = null;
        imgToPrint = null;
        locker = new ReentrantLock();
        imgMatrix = null;
    }

    // Metoda pro poslouchání kamery - funguje stejně jako onSurfaceTextureUpdated
    CameraCaptureSession.CaptureCallback CaptureCallback = new CameraCaptureSession.CaptureCallback() {
        @Override
        public void onCaptureCompleted(@NonNull CameraCaptureSession session,
                                       @NonNull CaptureRequest request,
                                       @NonNull TotalCaptureResult result) {
            //System.out.println("\n Captured");
        }
    };

    TextureView.SurfaceTextureListener textureListener = new TextureView.SurfaceTextureListener() {
        @Override
        public void onSurfaceTextureAvailable(SurfaceTexture surfaceTexture, int i, int i1) {
            imageRotation(privateTextureView.getWidth(), privateTextureView.getHeight());
            openCamera();
            System.out.println("\n ready Availble");
            //privateImageView.setAlpha(.50f);
        }

        @Override
        public void onSurfaceTextureSizeChanged(SurfaceTexture surfaceTexture, int i, int i1) {

        }

        @Override
        public boolean onSurfaceTextureDestroyed(SurfaceTexture surfaceTexture) {
            return false;
        }

        @Override
        public void onSurfaceTextureUpdated(SurfaceTexture surfaceTexture) {
            // Příprava cca 25ms

            // Komunikace s ImageProcessing vláknem
            boolean ready;
            locker.lock();
            ready = imgReady;
            locker.unlock();

            // Debug mod
            if(printImg) {
                privateImageView.setImageBitmap(imgToPrint);
                printImg = false;
            }

            if(!ready) {
                // Žádost pro vykreslení
                String str = "Objektů: "+ objectsCount +"    Čar: "+ linesCount;
                privateTextView.setText(str);

                finishedInMin = (finishedIn < finishedInMin && finishedIn>0)?finishedIn : finishedInMin;
                finishedInMax = (finishedIn > finishedInMax)?finishedIn : finishedInMax;
                String strTimers = finishedIn + "ms    " + finishedInMin +"/"+ finishedInMax + "ms";
                privateTextViewTimers.setText(strTimers);

                // Vykreslení výsledků
                privateImageView.setImageBitmap(imgToPrint);

                if(imgMatrix == null) {
                    // Pro první spuštění
                    System.out.println("\n První spuštění!! \n");
                    imfToProcess = privateTextureView.getBitmap();
                    imgMatrix = privateTextureView.getTransform( null );

                }

                // Převzetí nové bitmapy ke zpracování
                imfToProcess = privateTextureView.getBitmap();
                /*
                // Demo obrazek
                Bitmap b = privateTextureView.getBitmap();
                imfToProcess = Bitmap.createScaledBitmap(demoImage, b.getWidth(), b.getHeight(), true);
                */
                imgReady = true;
            }
        }
    };


    private void openCamera() {
        try {
            // Příprava vlákna
            runImfProcess = true;
            imageThread = new Thread(new ImageProcessing(), "Image proccess thread");
            imageThread.start();
        } catch (Exception ex) {
            Toast.makeText(KameraTwoActivity.this, "Nepovedlo se vytvořit Image proccess thread", Toast.LENGTH_SHORT).show();
        }
        try {
            // Najití sensoru - kamery a všechny dostupné úkony
            CameraManager manager = (CameraManager) getSystemService(Context.CAMERA_SERVICE);
            cameraId = manager.getCameraIdList()[0];

            // Získá parametry kamery
            CameraCharacteristics characteristics = manager.getCameraCharacteristics(cameraId);
            StreamConfigurationMap map = characteristics.get(CameraCharacteristics.SCALER_STREAM_CONFIGURATION_MAP);

            // assert - slouží pro určení statement
            assert map != null;

            // Zjištění rozlišení kamery
            cameraDimension = map.getOutputSizes(SurfaceTexture.class)[0];

            // Zjištění orientace
            /*
            CameraCharacteristics cameraCharacteristics = manager.getCameraCharacteristics(cameraId);
            int deviceOrientation = getWindowManager().getDefaultDisplay().getRotation();
            mTotalRotation = sensorToDeviceRotation(cameraCharacteristics, deviceOrientation);
            */
            // Vytiskne se v záložce "X: Run"
            System.out.println("\n Char: " + characteristics + "\n map: " + map);
            System.out.println("\n CameraID: " + cameraId + " /// rozlišení: " + cameraDimension);

            // Zjištění oprávnění k přistupu ke kameře pro API >=23
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this, new String[]{
                        Manifest.permission.CAMERA,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE
                }, REQUEST_CAMERA_PERMISSION);
                return;
            }

            // Otevření kamery s cameraId a poslání do funkce stateCallback, ta zavolá createCameraPreview
            manager.openCamera(cameraId, stateCallback, null);

        } catch (CameraAccessException e) {
            e.printStackTrace();
            return;
        }
    }

    private void setUpCameraPreview() {
        try {
            // Získání surface z objektu TextureView
            SurfaceTexture texture = privateTextureView.getSurfaceTexture();
            assert texture != null;
            texture.setDefaultBufferSize(cameraDimension.getWidth(), cameraDimension.getHeight());
            Surface surface = new Surface(texture);

            System.out.println("\n texture: " + texture + "\n surface: " + surface + "\n \n");

            // Vytvoření požadavku a přiřazení cíle našeho surface
            captureRequestBuilder = cameraDevice.createCaptureRequest(CameraDevice.TEMPLATE_PREVIEW);
            captureRequestBuilder.addTarget(surface);

            // Další parametry pro pořízení snímku
            final ImageReader reader = ImageReader.newInstance(cameraDimension.getWidth(), cameraDimension.getHeight(), ImageFormat.JPEG, 1);
            final CaptureRequest.Builder captureBuilder = cameraDevice.createCaptureRequest(CameraDevice.TEMPLATE_STILL_CAPTURE);
            captureBuilder.addTarget(reader.getSurface());

            // Vytvoření události pro získávání sekvenčních snímků
            cameraDevice.createCaptureSession(Arrays.asList(surface), new CameraCaptureSession.StateCallback() {
                @Override
                public void onConfigured(@NonNull CameraCaptureSession cameraCaptureSession) {
                    if (cameraDevice == null)
                        return;
                    cameraCaptureSessions = cameraCaptureSession;
                    cameraVideo();
                }

                @Override
                public void onConfigureFailed(@NonNull CameraCaptureSession cameraCaptureSession) {
                    Toast.makeText(KameraTwoActivity.this, "Changed", Toast.LENGTH_SHORT).show();
                }
            }, null);
        } catch (CameraAccessException e) {
            e.printStackTrace();
        }
    }

    private void cameraVideo() {
        if (cameraDevice == null)
            Toast.makeText(this, "Error", Toast.LENGTH_SHORT).show();
        captureRequestBuilder.set(CaptureRequest.CONTROL_MODE, CaptureRequest.CONTROL_MODE_AUTO);
        try {
            // zacyklení pořizování snímků - video

            cameraCaptureSessions.setRepeatingRequest(captureRequestBuilder.build(), CaptureCallback, mBackgroundHandler);
        } catch (CameraAccessException e) {
            e.printStackTrace();
        }
    }


    // Případ, kdy jsou permission zamítnuty
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == REQUEST_CAMERA_PERMISSION) {
            if (grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(this, "Nemužeš použít kameru dokud aplikace nezíská oprávnění", Toast.LENGTH_SHORT).show();
                finish();
            }
        }
    }


    // Slouží k převodu hodnot senzoru kamery na hodnoty senzoru zařízení
    private static SparseIntArray ORIENTATIONS = new SparseIntArray();

    static {
        ORIENTATIONS.append(Surface.ROTATION_0, 0);
        ORIENTATIONS.append(Surface.ROTATION_90, 90);
        ORIENTATIONS.append(Surface.ROTATION_180, 180);
        ORIENTATIONS.append(Surface.ROTATION_270, 270);
    }

    private static int sensorToDeviceRotation(CameraCharacteristics cameraCharacteristics, int deviceOrientation) {
        int sensorOrienatation = cameraCharacteristics.get(CameraCharacteristics.SENSOR_ORIENTATION);
        deviceOrientation = ORIENTATIONS.get(deviceOrientation);
        return (sensorOrienatation + deviceOrientation + 360) % 360;
    }


    public void onClick(View arg0) {
        // On click event obrazovky
        if (!debugModeToggleCircles) {
            debugModeToggleCircles = true;String str = "Debug mod: \"Circles\"";
            Toast.makeText(this, str, Toast.LENGTH_SHORT).show();
            privateTextViewLeftText.setText(str);
        }
        else if(!debugModeToggleLines) {
            debugModeToggleLines = true;
            String str = "Debug mod: \"Want see something\"";
            Toast.makeText(this, str, Toast.LENGTH_SHORT).show();
            privateTextViewLeftText.setText(str);
            privateTextViewLeftText.setTextColor(Color.BLACK);
        } else if(!debugModeToggleStepByStep) {
            debugModeToggleStepByStep = true;
            String str = "Debug mod: Developer";
            Toast.makeText(this, str, Toast.LENGTH_SHORT).show();
            privateTextViewLeftText.setText(str);
            privateTextViewLeftText.setTextColor(getColor(R.color.darkGreen));
        } else {
            debugModeToggleCircles = false;
            debugModeToggleLines = false;
            debugModeToggleStepByStep = false;
            String str = "Debug mod: OFF";
            Toast.makeText(this, str, Toast.LENGTH_SHORT).show();
            privateTextViewLeftText.setText(str);
            privateTextViewLeftText.setTextColor(getColor(R.color.darkRed));
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        startBackgroundThread();
        if (privateTextureView.isAvailable()) {
            imageRotation(privateTextureView.getWidth(), privateTextureView.getHeight());
            openCamera();
        } else
            privateTextureView.setSurfaceTextureListener(textureListener);
    }

    @Override
    protected void onPause() {
        closeCamera();
        stopBackgroundThread();
        super.onPause();
    }

    public void closeCamera() {
        if (cameraDevice != null) {
            cameraDevice.close();
            cameraDevice = null;
            try {
                runImfProcess = false;
                imageThread.join();
                imageThread = null;
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    private void stopBackgroundThread() {
        mBackgroundThread.quitSafely();
        try {
            mBackgroundThread.join();
            mBackgroundThread = null;
            mBackgroundHandler = null;
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private void startBackgroundThread() {
        mBackgroundThread = new HandlerThread("Camera Background");
        mBackgroundThread.start();
        mBackgroundHandler = new Handler(mBackgroundThread.getLooper());
    }

    private void imageRotation(int width, int height) {
        if (privateTextureView == null) {
            return;
        }

        Matrix matrix = new Matrix();
        int rotation = getWindowManager().getDefaultDisplay().getRotation();
        RectF textureRectF = new RectF(0, 0, width, height);
        RectF previewRectF = new RectF(0, 0, privateTextureView.getHeight(), privateTextureView.getWidth());
        float centerX = textureRectF.centerX();
        float centerY = textureRectF.centerY();
        if (rotation == Surface.ROTATION_90 || rotation == Surface.ROTATION_270) {
            previewRectF.offset(centerX - previewRectF.centerX(),
                    centerY - previewRectF.centerY());
            matrix.setRectToRect(textureRectF, previewRectF, Matrix.ScaleToFit.FILL);
            float scale = Math.max((float) width / privateTextureView.getWidth(),
                    (float) height / privateTextureView.getHeight());
            matrix.postScale(scale, scale, centerX, centerY);
            matrix.postRotate(90 * (rotation - 2), centerX, centerY);
        }
        //imgMatrix = matrix;
        privateTextureView.setTransform(matrix);
    }
}