package cz.retych.kamera_ii;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.Paint;

import java.nio.ByteBuffer;
import java.util.Random;

public class ImageProcessing extends KameraTwoActivity implements Runnable  {

    private Bitmap bmpInput;
    private Bitmap bmpResult;
    private int bmpWidth=0;
    private int bmpHeight=0;
    private int bmpBytesWidth=0;
    private Canvas ODcanvas;
    private Paint p;
    private Paint p2;
    private Paint p3;
    private Paint GreyScalePaint;

    private byte[] byteArray;
    private byte maxDiff = 30;

    public ImageProcessing() {
        GreyScalePaint = new Paint();
        ColorMatrix colorMatrix = new ColorMatrix();
        colorMatrix.setSaturation(0);
        ColorMatrixColorFilter privateFilter = new ColorMatrixColorFilter(colorMatrix);
        GreyScalePaint.setColorFilter(privateFilter);
        this.applyConBri(5,5);

        p3 = new Paint();
        p3.setStyle(Paint.Style.FILL);
        p3.setColor(Color.RED);
        p3.setStrokeWidth(3);

        p = new Paint();
        p.setStyle(Paint.Style.STROKE);
        p.setColor(Color.GREEN);
        p.setStrokeWidth(5);

        p2 = new Paint();
        p2.setStyle(Paint.Style.FILL);
        p2.setColor(Color.BLUE);
        p2.setStrokeWidth(1);
    }

    public void applyConBri(int contrast, int brightness) {
        ColorMatrix cm2 = new ColorMatrix(new float[] {
                        contrast, 0, 0, 0, brightness,
                        0, contrast, 0, 0, brightness,
                        0, 0, contrast, 0, brightness,
                        0, 0, 0, 1, 0 });
        Paint conBriPaint = new Paint();
        conBriPaint.setColorFilter(new ColorMatrixColorFilter(cm2));
    }

    private int getYFromArray(int position) {
        return position / this.bmpWidth / 4;
    }
    
    private int getXFromArray(int position) {
        return (position - getYFromArray(position) * this.bmpWidth * 4) / 4;
    }
    
    private void debugMode(int position, int cl) {
        if(debugModeToggleStepByStep) {
            p3.setColor(cl);
            Bitmap saveBitmap = Bitmap.createBitmap(this.bmpInput);
            ODcanvas.drawCircle((float) getXFromArray(position), (float) getYFromArray(position), 5f, p3);
            imgToPrint = this.bmpInput;
            printImg = true;

            try {
                Thread.sleep(200);
                this.bmpInput = Bitmap.createBitmap(saveBitmap);
                ODcanvas = new Canvas(this.bmpInput);
                imgToPrint = this.bmpInput;
                printImg = true;
            } catch (Exception ex) {
                System.out.print(" \n Sleep problems \n");
            }
        }
    }

    private void debugModeColorPath(int position) {
        if(debugModeToggleStepByStep) {
            p3.setColor(Color.YELLOW);
            ODcanvas.drawCircle((float) getXFromArray(position), (float) getYFromArray(position), 5f, p3);
            /*try {
                Thread.sleep(500);
            } catch (Exception ex) {

            }*/
        }
    }
    private boolean checkPixel(int position, int startPos, boolean checked, int startValue, int color) {
        debugMode(position, color);

        if(position == startPos) {
            debugModeColorPath(position);
            return true;
        }

        if(!checked) {
            debugMode(position, color);
            int stepValue = byteArray[position] & 0xFF;
            if (((startValue - maxDiff) < stepValue) && (stepValue < (startValue + maxDiff))) {
                // Naleží v rozmezí - shodný odstín
                debugModeColorPath(position);
                return true;
            }
        }
        return false;
    }

    private void customObjectDetection() {
        linesCount = 0;
        objectsCount = 0;

        // Přířazení reference na bitmapu Canvasu
        ODcanvas = new Canvas(this.bmpInput);
        // Aplikace GreyScale filtru
        ODcanvas.drawBitmap(this.bmpInput, 0, 0, GreyScalePaint);

        // Převod Bitmap -> biteArray
        int size = this.bmpBytesWidth * this.bmpHeight;
        ByteBuffer byteBuffer = ByteBuffer.allocate(size);
        this.bmpInput.copyPixelsToBuffer(byteBuffer);
        this.byteArray = byteBuffer.array();

        // Proměnné pro práci s OD
        byte rgbaIndex = 0;
        int moveStep = 30;
        // 1080*4*720 / 50 = 62 208
        // maxPoints = 774
        int maxPoints = (size / moveStep) / 100;
        float[] objectPointsX = new float[maxPoints];
        float[] objectPointsY = new float[maxPoints];
        boolean[] pointsChecked = new boolean[size];
        //List<Point> detectedObjects = new ArrayList<Point>();

        for (int j = moveStep; j < this.bmpHeight; j+= moveStep) {
            for (int i = moveStep; i < this.bmpWidth; i += moveStep) {

                boolean objectFound = false;

                // Vykreslení "koleček"
                if(debugModeToggleCircles) {
                    p.setColor(Color.WHITE);
                    ODcanvas.drawCircle(i, j, 2, p2);
                }

                // Inicializace začátku objektu
                int actualPosition = rgbaIndex + 4 * (i + (j * this.bmpWidth));
                int startPosition = actualPosition;
                int counter = 0;
                objectPointsX[counter] = i;
                objectPointsY[counter] = j;

                if(!pointsChecked[actualPosition]) {

                    // Reset hodnot
                    boolean found = false;
                    boolean whileEnder = false;
                    int stepPosition = 0;
                    int stepVal;

                    // Konfigurace pohybů
                    // 0: Nahoru, 1: Doprava, 2: Dolů, 3: Doleva
                    byte[] stepConfig = new byte[]{0,1,2,5};

                    while (!whileEnder && counter<maxPoints) {
                        // Hodnota zkoumaného objektu
                        int value = byteArray[actualPosition] & 0xFF;

                        // Zaznamenání, že jsme zde již byli
                        pointsChecked[actualPosition] = true;

                        // Ošetření při najití objektu
                        if(found && (startPosition == stepPosition)) {
                            objectsCount++;
                            objectFound = true;
                            counter++;
                            try {
                                // Uložení souřadnic
                                objectPointsX[counter] = getXFromArray(stepPosition);
                                objectPointsY[counter] = getYFromArray(stepPosition);
                                //System.out.println("\n \n  stepPosition/y/x:"+stepPosition + " / " + y + " / " + x + "\n");
                            } catch (Exception ex) {
                                //System.out.print("\n " + ex + "\n");
                            }
                            break;
                        }
                        // Reset přepínače najitého pixelu
                        found = false;

                        for (int step=0; step<stepConfig.length; step++) {
                            // Při najití pixelu
                            if (found) {
                                counter++;
                                try {
                                    // Uložení souřadnic
                                    objectPointsX[counter] = getXFromArray(stepPosition);
                                    objectPointsY[counter] = getYFromArray(stepPosition);
                                    //System.out.println("\n \n  stepPosition/y/x:"+stepPosition + " / " + y + " / " + x + "\n");
                                } catch (Exception ex) {
                                    //System.out.print("\n " + ex + "\n");
                                }
                                break;
                            }

                            try {
                                // Procházení skrze konfiguraci
                                switch (stepConfig[step]) {
                                    case 0: {
                                        //Nahoru
                                        stepPosition = actualPosition - moveStep * this.bmpWidth * 4;

                                        if(checkPixel(stepPosition, startPosition, pointsChecked[stepPosition], value, Color.BLACK)) {
                                            found = true;
                                            actualPosition = stepPosition;
                                            // Přenastavení konfigurace - směru při projití třemi směry
                                            // O 90° po směru hodinových ručiček

                                            if(step >=2) stepConfig = new byte[]{3, 0, 1, 5};
                                            // více se ztrácí, zato přesnější
                                            //if(step >=2 || step==0) stepConfig = new byte[]{3, 0, 1, 5};
                                        }
                                        break;
                                    } case 1: {
                                        //Doprava
                                        stepPosition = actualPosition + moveStep*4;
                                        if(getYFromArray(stepPosition) != getYFromArray(actualPosition)) break;

                                        if(checkPixel(stepPosition, startPosition, pointsChecked[stepPosition], value, Color.RED)) {
                                            found = true;
                                            actualPosition = stepPosition;
                                            if(step >=2) stepConfig = new byte[]{0, 1, 2, 5};
                                            //if(step >=2 || step==0) stepConfig = new byte[]{0, 1, 2, 5};
                                        }
                                        break;
                                    } case 2: {
                                        //Dolu
                                        stepPosition = actualPosition + moveStep * this.bmpWidth * 4;

                                        if(checkPixel(stepPosition, startPosition, pointsChecked[stepPosition], value, Color.BLUE)) {
                                            found = true;
                                            actualPosition = stepPosition;
                                            if(step >=2) stepConfig = new byte[]{1, 2, 3, 5};
                                            //if(step >=2 || step==0) stepConfig = new byte[]{1, 2, 3, 5};
                                        }
                                        break;
                                    } case 3: {
                                        //Doleva
                                        stepPosition = actualPosition - moveStep*4;
                                        if(getYFromArray(stepPosition) != getYFromArray(actualPosition)) break;

                                        if(checkPixel(stepPosition, startPosition, pointsChecked[stepPosition], value, Color.MAGENTA)) {
                                            found = true;
                                            actualPosition = stepPosition;
                                            if(step >=2) stepConfig = new byte[]{2, 3, 0, 5};
                                            //if(step >=2 || step==0) stepConfig = new byte[]{2, 3, 0, 5};
                                        }
                                        break;
                                    } case 5: {
                                        //Not found
                                        whileEnder = true;
                                        break;
                                    } default: {
                                        System.out.println("\n Default state -> something wrong");
                                        whileEnder = true;
                                        found = false;
                                        break;
                                    }
                                }
                                // SWITCH FINISH
                            } catch (Exception e) {
                                whileEnder = true;
                                found = false;
                            }
                        }
                    }
                    // WHILE FINISH

                    // Vykreslení čar a objektů
                    boolean question = false;
                    question = (debugModeToggleCircles) ? counter > 5 : objectFound;
                    if(question)
                    {
                        //detectedObjects.add(objectPoints);
                        if(objectFound) {
                            p.setColor(Color.GREEN);
                        } else {
                            Random random = new Random();
                            int r = random.nextInt(255);
                            int g = random.nextInt(60);
                            int b = random.nextInt(60);
                            p.setColor(Color.rgb(r, g, b));
                        }

                        for(int k=0; k<counter-1;k++)
                        {
                            // Kreslení čar
                            ODcanvas.drawLine(objectPointsX[k], objectPointsY[k], objectPointsX[k+1], objectPointsY[k+1], p);
                            // Debug mode (pomale vykreslovani)

                            // debug mod 1 - zobrazení vykreslování
                            if(debugModeToggleLines) {
                                imgToPrint = this.bmpInput;
                                printImg = true;
                                try {
                                    Thread.sleep(20);
                                } catch (Exception ex) {
                                    System.out.println("\n Chyba sleepu:\n\n" + ex);
                                }
                            }
                        }
                    }
                }
            }
        }
        //System.out.println("\n \n______Objects:" + objectCount+"\n  ______Lines:" + linesCount +"\n \n" + debugModeToggle +"??" +debugModeToggle2 + "\n\n");
    }

    @Override
    public void run() {
        while (runImfProcess) {
            try {
                boolean ready;
                locker.lock();
                ready=imgReady;
                locker.unlock();
                long startTime = System.currentTimeMillis();

                if(ready) {
                    if(this.bmpHeight == 0) {
                        // Nastavení při prvním startu
                        this.bmpWidth = imfToProcess.getWidth();
                        this.bmpHeight = imfToProcess.getHeight();
                        this.bmpBytesWidth = imfToProcess.getRowBytes();
                    }
                    // Aplikace rotace matrix filtru 100ms
                    this.bmpInput = Bitmap.createBitmap(imfToProcess, 0, 0,
                            this.bmpWidth, this.bmpHeight,
                            imgMatrix, true);
                    /*
                    // Verze pro překrytí
                    bmpResult = Bitmap.createBitmap(this.bmpWidth,
                            this.bmpHeight, Bitmap.Config.ARGB_8888);
                    Canvas canvas = new Canvas(bmpResult);
                    final Paint paint = new Paint();
                    paint.setAlpha(255);
                    canvas.drawBitmap(bmpResult, 0, 0, paint);
                    */

                    // Funkce pro OD
                    this.customObjectDetection();

                    long finalTime = System.currentTimeMillis()-startTime;
                    locker.lock();
                    // Poslání hodnot třídě s UI
                    //imgToPrint = this.bmpResult;
                    imgToPrint = this.bmpInput;
                    printImg = true;
                    imgReady = false;
                    finishedIn = finalTime;

                    locker.unlock();
                }
                try {
                    Thread.sleep(20);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}

