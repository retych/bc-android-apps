package cz.retych.kamera_i;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.widget.Toast;

import java.nio.ByteBuffer;
import java.util.Random;

public class ByteArrayFilters {
    private byte[] byteArray;
    private Bitmap bmp;
    int bmpWidth;
    int bmpHeight;
    int bmpBytesPerPixel;
    int actualPosition;


    // Filtrace - odstraneni jedne z barev
    // 4 bajty: R G B A
    // 4 bajty: 0 1 2 3
    // byte => -128 až 127
    // černá -> šedá -> šedá -> bílá
    //     0 ->  127 -> -128 -> -1

    ByteArrayFilters() {
        this.actualPosition = 0;
    }

    ByteArrayFilters(Bitmap bm) {
        this.bmp = bm;
        this.bmpWidth = this.bmp.getWidth();
        this.bmpHeight = this.bmp.getHeight();
        this.bmpBytesPerPixel = this.bmp.getRowBytes();
        // Převod Bitmap -> biteArray
        this.getArrayFromBitmap();
        this.actualPosition = 0;
    }

    public void setBitmap(Bitmap bm) {
        this.bmp = bm;
        this.bmpWidth = this.bmp.getWidth();
        this.bmpHeight = this.bmp.getHeight();
        this.bmpBytesPerPixel = this.bmp.getRowBytes();
        // Převod Bitmap -> biteArray
        this.getArrayFromBitmap();
        this.actualPosition = 0;
    }

    private void getArrayFromBitmap() {
        // Převod Bitmap -> byteArray
        int size = this.bmpBytesPerPixel * this.bmpHeight;
        ByteBuffer byteBuffer = ByteBuffer.allocate(size);
        this.bmp.copyPixelsToBuffer(byteBuffer);
        this.byteArray = byteBuffer.array();
    }

    private void getBitmapFromArray() {
        // Převod byteArray -> bitmap
        Bitmap.Config configBmp = Bitmap.Config.valueOf(this.bmp.getConfig().name());
        this.bmp = Bitmap.createBitmap(this.bmpWidth, this.bmpHeight, configBmp);
        ByteBuffer buffer = ByteBuffer.wrap(this.byteArray);
        this.bmp.copyPixelsFromBuffer(buffer);
        System.out.println("\n DONE "+ configBmp);
    }

    public Bitmap getBitmap() {
        getBitmapFromArray();
        return this.bmp;
    }

    public void raiseRBG(int rgbindex, int byteValue) {
        // Lze nahradit paint.setARGB(0, byteValue, 0, 0)
        if(rgbindex>=0 && rgbindex<5) {
            this.actualPosition = rgbindex;
            for (int j = 0; j < bmpHeight; j++) {
                for (int i = 0; i < (bmpBytesPerPixel / 4); i++) {
                    byteArray[actualPosition] = (byte)((byteArray[actualPosition]&0xFF) + byteValue);
                    //byteArray[actualPosition] = -1;
                    actualPosition += 4;
                }
            }
        } else {
            System.out.println("\n rgbvalue musí být mezi 0 až 4");
        }
    }

    public void dissableRBG(int rgbindex) {
        if(rgbindex>=0 && rgbindex<5) {
            this.actualPosition = rgbindex;
            for (int j = 0; j < bmpHeight; j++) {
                for (int i = 0; i < (bmpBytesPerPixel / 4); i++) {
                    byteArray[actualPosition] += 0;
                    actualPosition += 4;
                }
            }
        } else {
            System.out.println("\n rgbvalue musí být mezi 0 až 4");
        }
    }

    public void applyShadesOfGray() {
        this.actualPosition = 1; //Green
        for(int j=0; j < bmpHeight ; j++) {
            for(int i=0 ; i < bmpBytesPerPixel/4 ;i++) {

                byte midColor =  (byte)( (byteArray[actualPosition-1]&0xFF) * 0.30 + (byteArray[actualPosition]&0xFF) * 0.59 + (byteArray[actualPosition+1]&0xFF) * 0.11 );
                byteArray[actualPosition]=midColor;
                byteArray[actualPosition+1]=midColor;
                byteArray[actualPosition-1]=midColor;

                //byteArray[actualPosition+1]=byteArray[actualPosition];
                //byteArray[actualPosition+-1]=byteArray[actualPosition];
                actualPosition += 4;
            }
        }
    }
}
