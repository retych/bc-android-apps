package cz.retych.kamera_i;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.ImageFormat;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.SurfaceTexture;
import android.graphics.drawable.BitmapDrawable;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraCaptureSession;
import android.hardware.camera2.CameraCharacteristics;
import android.hardware.camera2.CameraDevice;
import android.hardware.camera2.CameraManager;
import android.hardware.camera2.CaptureRequest;
import android.hardware.camera2.params.StreamConfigurationMap;
import android.media.ImageReader;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.HandlerThread;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Size;
import android.view.Surface;
import android.view.TextureView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;
import java.util.Arrays;

public class KameraActivity extends AppCompatActivity {

    // Komponenty UI
    private TextureView privateTextureView;
    private ImageView privateImageView;
    private ImageView demoImageView;
    private Bitmap pictureBitmap;

    // Proměnné potřebné pro práci s kamera2 api
    private CameraDevice cameraDevice;
    private CameraCaptureSession cameraCaptureSessions;
    private CaptureRequest.Builder captureRequestBuilder;
    private Size cameraDimension;
    private static final int REQUEST_CAMERA_PERMISSION = 200;
    private Handler mBackgroundHandler;
    private HandlerThread mBackgroundThread;
    private long AsyncTimer;

    // Vytvoření tvz. call-backu(zpětné odezvy) pro práci s kamera2 api
    CameraDevice.StateCallback stateCallback = new CameraDevice.StateCallback() {
        @Override
        public void onOpened(@NonNull CameraDevice camera) {
            cameraDevice = camera;
            setUpCameraPreview();
        }

        @Override
        public void onDisconnected(@NonNull CameraDevice cameraDevice) {
            cameraDevice.close();
        }

        @Override
        public void onError(@NonNull CameraDevice cameraDevice, int i) {
            cameraDevice.close();
            cameraDevice = null;
        }
    };

    //Funkce onCreate volající se při startu programu - inicializace
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_kamera);

        privateTextureView = (TextureView) findViewById(R.id.textureView);

        // Od Java 1.4 ,Lze použít 'assert' k zjištění true nebo false
        assert privateTextureView != null;

        // Vytvoření posluchače pro privateTextureView a button
        privateTextureView.setSurfaceTextureListener(textureListener);
        Button privateBtnGetCapture = findViewById(R.id.buttonGetPicture);
        privateImageView = findViewById(R.id.imageViewResult);
        pictureBitmap = null;


        // Pro Demo image -> Papoušek - zakomentovat invisible a odkomentovat řádek 110
        /*
        demoImageView = findViewById(R.id.demoImageView);
        //demoImageView.setVisibility(View.INVISIBLE);
        */

        privateBtnGetCapture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                System.out.println("\n Clicked ");
                pictureBitmap = privateTextureView.getBitmap();
                AsyncTimer = System.currentTimeMillis();

                // Demo
                //pictureBitmap =((BitmapDrawable)demoImageView.getDrawable()).getBitmap();

                //Přiřazení bitmapy do ImageView
                //AsyncTask<Bitmap, Bitmap, Bitmap> myTask = new ApplyFilterTask();
                //Start MyTask
                //myTask.execute(pictureBitmap);

                //Jednodušší varianta
                new ApplyFilterTask().execute(pictureBitmap);
            }
        });
    }

    // Listener na surfaceTexture TextureView
    TextureView.SurfaceTextureListener textureListener = new TextureView.SurfaceTextureListener() {
        @Override
        public void onSurfaceTextureAvailable(SurfaceTexture surfaceTexture, int i, int i1) {
            transformImage(privateTextureView.getWidth(), privateTextureView.getHeight());
            openCamera();
        }

        @Override
        public void onSurfaceTextureSizeChanged(SurfaceTexture surfaceTexture, int i, int i1) {

        }

        @Override
        public boolean onSurfaceTextureDestroyed(SurfaceTexture surfaceTexture) {
            return false;
        }

        @Override
        public void onSurfaceTextureUpdated(SurfaceTexture surfaceTexture) {
        }
    };

    private void openCamera() {
        // Najití sensoru - kamery a všechny dostupné úkony
        CameraManager manager = (CameraManager) getSystemService(Context.CAMERA_SERVICE);
        try {
            String cameraId = manager.getCameraIdList()[0];

            // Získá parametry kamery
            CameraCharacteristics characteristics = manager.getCameraCharacteristics(cameraId);
            StreamConfigurationMap map = characteristics.get(CameraCharacteristics.SCALER_STREAM_CONFIGURATION_MAP);

            // assert - slouží pro určení statement
            assert map != null;

            // Zjištění rozlišení kamery
            cameraDimension = map.getOutputSizes(SurfaceTexture.class)[0];

            // Tisk Info
            System.out.println("\n Char: " + characteristics + "\n map: " + map);
            System.out.println("\n CameraID: " + cameraId + " /// rozlišení: " + cameraDimension);

            // Zjištění oprávnění k přistupu ke kameře pro API >=23
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this, new String[]{
                        Manifest.permission.CAMERA,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE
                }, REQUEST_CAMERA_PERMISSION);
                return;
            }

            // Otevření kamery s cameraId a poslání do funkce stateCallback, ta zavolá createCameraPreview
            manager.openCamera(cameraId, stateCallback, null);

        } catch (CameraAccessException e) {
            e.printStackTrace();
        }
    }

    private void setUpCameraPreview() {
        try {
            // Získání surface z objektu TextureView
            SurfaceTexture texture = privateTextureView.getSurfaceTexture();
            assert texture != null;
            texture.setDefaultBufferSize(cameraDimension.getWidth(), cameraDimension.getHeight());
            Surface surface = new Surface(texture);

            System.out.println("\n texture: " + texture + "\n surface: " + surface + "\n \n");

            // Vytvoření požadavku a přiřazení cíle našeho surface
            captureRequestBuilder = cameraDevice.createCaptureRequest(CameraDevice.TEMPLATE_PREVIEW);
            captureRequestBuilder.addTarget(surface);

            // Další parametry pro pořízení snímku
            final ImageReader reader = ImageReader.newInstance(cameraDimension.getWidth(), cameraDimension.getHeight(), ImageFormat.JPEG, 1);
            final CaptureRequest.Builder captureBuilder = cameraDevice.createCaptureRequest(CameraDevice.TEMPLATE_STILL_CAPTURE);
            captureBuilder.addTarget(reader.getSurface());

            // Vytvoření události pro získávání sekvenčních snímků
            cameraDevice.createCaptureSession(Arrays.asList(surface), new CameraCaptureSession.StateCallback() {
                @Override
                public void onConfigured(@NonNull CameraCaptureSession cameraCaptureSession) {
                    if (cameraDevice == null)
                        return;
                    cameraCaptureSessions = cameraCaptureSession;
                    cameraVideo();
                }

                @Override
                public void onConfigureFailed(@NonNull CameraCaptureSession cameraCaptureSession) {
                    Toast.makeText(KameraActivity.this, "Changed", Toast.LENGTH_SHORT).show();
                }
            }, null);
        } catch (CameraAccessException e) {
            e.printStackTrace();
        }
    }

    private void cameraVideo() {
        if (cameraDevice == null)
            Toast.makeText(this, "Error", Toast.LENGTH_SHORT).show();
        captureRequestBuilder.set(CaptureRequest.CONTROL_MODE, CaptureRequest.CONTROL_MODE_AUTO);
        try {
            // zacyklení pořizování snímků - video
            cameraCaptureSessions.setRepeatingRequest(captureRequestBuilder.build(), null, mBackgroundHandler);
        } catch (CameraAccessException e) {
            e.printStackTrace();
        }
    }

    // AsyncTask
    protected class ApplyFilterTask extends AsyncTask<Bitmap, Bitmap, Void> {

        ByteArrayFilters filter = new ByteArrayFilters();

        @Override
        protected void onPreExecute() {
            // Interakce s UI
            int color = Color.parseColor("#80660000");
            privateImageView.setColorFilter(color);

            // Grey scale pomocí filtru alias překryvné vrstvy (5ms)
            // Neprovede změnu samotného obrazu
            //privateImageView.setColorFilter(privateFilter);
        }

        @Override
        protected Void doInBackground(Bitmap... bitmaps) {
            try {
                // TIMER
                long StartTime = System.currentTimeMillis();

                // Načtení bitmapy ze TextureView
                //System.out.println("\n bitmaps: " +bitmaps.length+"\n");
                filter.setBitmap(bitmaps[0]);

                // 4 bajty: R G B A
                // 4 bajty: 0 1 2 3
                // byte => -128 až 127
                // černá -> šedá -> šedá -> bílá
                //     0 ->  127 -> -128 -> -1
                filter.raiseRBG(1, 5);

                // Filtrace - odstraneni jedne z barev
                //filter.dissableRBG(0);

                //Grey scale pomocí upravování pixelů (~3500ms)
                filter.applyShadesOfGray();

                long Timer2 = System.currentTimeMillis();
                long functionTime = Timer2 - StartTime;

                publishProgress(filter.getBitmap());
                //publishProgress(bitmaps[0]);

                long stopTime = System.currentTimeMillis();
                long TotalTime = stopTime - StartTime;
                //timer vypis
                System.out.println("\n TotalTime: " + TotalTime + "ms" + " / functionTime" + functionTime + "ms");

            } catch (Exception e) {
                System.out.println("CHYBA AsyncTask doInBackground! -> " + e);
            }

            return null;
        }

        @Override
        protected void onProgressUpdate(Bitmap... bitmaps) {
            try {
                // Interakce s UI
                System.out.println("ProgressUpdating");

                // Přepis původní bitmapy
                privateImageView.setImageBitmap(bitmaps[0]);
                privateImageView.clearColorFilter();

                // Aplikace GreyScale filtru metoda s 20ms

                // Příprava grey scale filtru pro metoda s časem 20ms -> onProgressUpdate
                /*
                Paint greyScalePaint = new Paint();
                ColorMatrix cm = new ColorMatrix();
                cm.setSaturation(0);
                ColorMatrixColorFilter privateFilter = new ColorMatrixColorFilter(cm);
                greyScalePaint.setColorFilter(privateFilter);

                Bitmap bm = Bitmap.createBitmap(bitmaps[0].getWidth(), bitmaps[0].getHeight(),
                        Bitmap.Config.ARGB_8888);
                Canvas c = new Canvas(bm);
                c.drawBitmap(bitmaps[0], 0, 0, greyScalePaint);

                //bitmaps[0].recycle();
                privateImageView.setImageBitmap(bm);
                privateImageView.clearColorFilter();
                */

                long stopTime = System.currentTimeMillis();
                long TotalTime = stopTime - AsyncTimer;

                // Výpis na obrazovku a do konzole
                String resultText = "Celkový čas: " + TotalTime + "ms";
                Toast.makeText(getApplicationContext(), resultText, Toast.LENGTH_LONG).show();
                System.out.println("\n" + resultText);
            }catch (Exception ex) {
                System.out.println("\n" + ex);
            }
        }
    }

    // Případ, kdy jsou permission zamítnuty
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if(requestCode == REQUEST_CAMERA_PERMISSION)
        {
            if(grantResults[0] != PackageManager.PERMISSION_GRANTED)
            {
                Toast.makeText(this, "Nemužeš použít kameru dokud aplikace nezíská oprávnění", Toast.LENGTH_SHORT).show();
                finish();
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        startBackgroundThread();
        if(privateTextureView.isAvailable()) {
            transformImage(privateTextureView.getWidth(), privateTextureView.getHeight());
            openCamera();
        } else
            privateTextureView.setSurfaceTextureListener(textureListener);
    }

    @Override
    protected void onPause() {
        closeCamera();
        stopBackgroundThread();
        super.onPause();
    }

    public void closeCamera() {
        if (cameraDevice != null) {
            cameraDevice.close();
            cameraDevice = null;
        }
    }
    private void stopBackgroundThread() {
        mBackgroundThread.quitSafely();
        try{
            mBackgroundThread.join();
            mBackgroundThread = null;
            mBackgroundHandler = null;
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private void startBackgroundThread() {
        mBackgroundThread = new HandlerThread("Camera Background");
        mBackgroundThread.start();
        mBackgroundHandler = new Handler(mBackgroundThread.getLooper());
    }

    private void transformImage(int width, int height) {
        if(privateTextureView == null) {
            return;
        }

        Matrix matrix = new Matrix();
        int rotation = getWindowManager().getDefaultDisplay().getRotation();
        RectF textureRectF = new RectF(0, 0, width, height);
        RectF previewRectF = new RectF(0, 0, privateTextureView.getHeight(), privateTextureView.getWidth());
        float centerX = textureRectF.centerX();
        float centerY = textureRectF.centerY();
        if(rotation == Surface.ROTATION_90 || rotation == Surface.ROTATION_270) {
            previewRectF.offset(centerX - previewRectF.centerX(),
                    centerY - previewRectF.centerY());
            matrix.setRectToRect(textureRectF, previewRectF, Matrix.ScaleToFit.FILL);
            float scale = Math.max((float)width / privateTextureView.getWidth(),
                    (float)height / privateTextureView.getHeight());
            matrix.postScale(scale, scale, centerX, centerY);
            matrix.postRotate(90 * (rotation - 2), centerX, centerY);
        }
        privateTextureView.setTransform(matrix);
    }
}

